# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2024-04-01
### Breaking Changes
- Made storytempl.Storybook private fields public to simplify code and remove getter methods.

### Fixed
- Use static test time value with dateArgDemo to avoid needless Storybook static rebuild.
- Fix some log messages

### Added
- Added some test cases to prove components render as expected

## [0.1.0] - 2024-03-27

### Added
- Initial release

[0.2.0]: https://gitlab.com/joeslazaro/storytempl/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/joeslazaro/storytempl/-/tags/v0.1.0
