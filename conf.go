package storytempl

// SPDX-License-Identifier: MIT

import (
	"errors"
	"fmt"

	"github.com/gosimple/slug"
	"gitlab.com/joeslazaro/storytempl/util"
)

// This is the "real" config that will get turned into a *_stories.json file.
type Conf struct {
	Title      string                                `json:"title"`
	Parameters storyParameters                       `json:"parameters"`
	Args       *util.OrderedMap[string, interface{}] `json:"args"`
	ArgTypes   *util.OrderedMap[string, interface{}] `json:"argTypes"`
	Stories    []story                               `json:"stories"`
	argTypeMap map[string]ArgType
}

type story struct {
	Name string                                `json:"name"`
	Args *util.OrderedMap[string, interface{}] `json:"args,omitempty"`
}

type storyParameters struct {
	Server map[string]interface{} `json:"server"`
}

func NewConf(title string) *Conf {
	conf := &Conf{
		Title: title,
		Parameters: storyParameters{
			Server: map[string]interface{}{
				"id": slug.Make(title),
			},
		},
		Args:       util.NewOrderedMap[string, interface{}](),
		ArgTypes:   util.NewOrderedMap[string, interface{}](),
		Stories:    []story{},
		argTypeMap: make(map[string]ArgType),
	}

	return conf
}

func (cb *Conf) SlugTitle() string {
	return slug.Make(cb.Title)
}

func (cb *Conf) AddArgTypes(argTypes ...ArgType) *Conf {
	for _, argT := range argTypes {
		_, exists := cb.ArgTypes.Get(argT.Name)
		if exists {
			panic(fmt.Sprintf("Tried to add argType %s which already exists",
				argT.Name))
		}
		argTypeValue := map[string]interface{}{
			"control": argT.Control,
		}
		if argT.Options != nil {
			argTypeValue["options"] = argT.Options
		}
		cb.ArgTypes.Add(argT.Name, argTypeValue)
		cb.argTypeMap[argT.Name] = argT
	}
	return cb
}

func (cb *Conf) AddDefaultArgs(args ...Arg) *Conf {
	for _, arg := range args {
		if _, exists := cb.Args.Get(arg.Name); exists {
			panic(fmt.Sprintf("Tried to add global arg %s which already exists",
				arg.Name))
		}
		cb.Args.Add(arg.Name, arg.Value)
	}
	return cb
}

func (cb *Conf) AddStory(name string, args ...Arg) *Conf {
	storyArgs := util.NewOrderedMap[string, interface{}]()
	for _, arg := range args {
		if _, exists := storyArgs.Get(arg.Name); exists {
			panic(fmt.Sprintf(
				"In story %s, tried to add story arg %s which already exists",
				name, arg.Name))
		}
		storyArgs.Add(arg.Name, arg.Value)
	}
	if len(storyArgs.Keys()) == 0 {
		storyArgs = nil
	}
	cb.Stories = append(cb.Stories, story{
		Name: name,
		Args: storyArgs,
	})
	return cb
}

func (conf *Conf) Validate() error {
	if len(conf.Stories) == 0 {
		return errors.New("no stories defined")
	}

	if err := conf.validateArgs(conf.Args); err != nil {
		return err
	}

	for _, story := range conf.Stories {
		if story.Args == nil {
			continue
		}
		if err := conf.validateArgs(story.Args); err != nil {
			return err
		}
	}

	return nil
}

func (conf *Conf) validateArgs(args *util.OrderedMap[string, interface{}]) error {
	for _, argName := range args.Keys() {
		argType, ok := conf.argTypeMap[argName]
		if !ok {
			return fmt.Errorf("no ArgType defined for %s", argName)
		}
		argValue, exists := args.Get(argName)
		if !exists {
			return fmt.Errorf("missing value for arg %s", argName)
		}
		if argType.Validate != nil {
			err := argType.Validate(argValue)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
