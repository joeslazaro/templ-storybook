package components

import "gitlab.com/joeslazaro/storytempl"

func BooleanArgDemoStoryConf() *storytempl.Conf {
	return storytempl.NewConf("Simple/BooleanArg").
		AddArgTypes(storytempl.BooleanArgType("checked")).
		AddDefaultArgs(storytempl.BooleanArg("checked", false)).
		AddStory("Default")
}
