package components

import "gitlab.com/joeslazaro/storytempl"

func CheckArgDemoStoryConf() *storytempl.Conf {
	return storytempl.NewConf("Simple/CheckArg").
		AddArgTypes(
			storytempl.CheckArgType("checkValue",
				[]string{"foo", "bar", "baz"}),
			storytempl.CheckInlineArgType("inlineCheckValue",
				[]string{"flim", "flam", "floop"}),
		).
		AddStory("Default")
}
