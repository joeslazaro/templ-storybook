package components

import (
	"time"

	"gitlab.com/joeslazaro/storytempl"
)

func DateArgDemoStoryConf() *storytempl.Conf {
	testTime := time.Date(2024, time.April, 1, 11, 38, 19, 366803,
		time.FixedZone("MST", -7*60*60))
	return storytempl.NewConf("Simple/DateArg").
		AddArgTypes(storytempl.DateArgType("when")).
		AddDefaultArgs(storytempl.DateArg("when", testTime)).
		AddStory("Default")
}
