package components

import "gitlab.com/joeslazaro/storytempl"

func TextArgDemoStoryConf() *storytempl.Conf {
	return storytempl.NewConf("Simple/TextArg").
		AddArgTypes(
			storytempl.TextArgType("placeholder"),
			storytempl.TextArgType("value1"),
			// Note: maxlen only applies to the input in Storybook's Control panel
			storytempl.TextArgMaxLenType("value2", 6),
		).
		AddStory("Default",
			storytempl.TextArg("placeholder", "Type something here"),
			storytempl.TextArg("value1", ""),
			storytempl.TextArg("value2", ""),
		)
}
