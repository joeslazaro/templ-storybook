package components

import "gitlab.com/joeslazaro/storytempl"

func NumberArgDemoStoryConf() *storytempl.Conf {
	return storytempl.NewConf("Simple/Number-and-Range-args").
		AddArgTypes(
			storytempl.TextArgType("text"),
			storytempl.NumberArgType("count1",
				storytempl.NewNumberArgConf().SetMax(4)),
			storytempl.NumberArgType("count2", nil), // Use defaults
			storytempl.RangeArgType("count3",
				storytempl.NewNumberArgConf().SetMin(2).SetMax(10).SetStep(2)),
		).
		AddDefaultArgs(
			storytempl.TextArg("text", "Is there an echo in here?"),
			storytempl.NumberArg("count1", 1),
			storytempl.NumberArg("count2", 1),
			storytempl.NumberArg("count3", 2),
		).
		AddStory("Default")
}
