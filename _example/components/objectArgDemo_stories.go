package components

import "gitlab.com/joeslazaro/storytempl"

type ObjectArgStoryDemoType struct {
	Title  string
	Author string
}

func ObjectArgDemoStoryConf() *storytempl.Conf {
	return storytempl.NewConf("Simple/ObjectArg").
		AddArgTypes(
			storytempl.ObjectArgType("bookInfo", &ObjectArgStoryDemoType{}),
		).
		AddDefaultArgs(
			storytempl.ObjectArg("bookInfo", ObjectArgStoryDemoType{
				Title:  "1984",
				Author: "Orwell",
			}),
		).
		AddStory("Default")
}
