package components

import "gitlab.com/joeslazaro/storytempl"

func RadioArgDemoStoryConf() *storytempl.Conf {
	return storytempl.NewConf("Simple/RadioArg").
		AddArgTypes(
			storytempl.RadioArgType("radioValue",
				[]string{"foo", "bar", "baz"}),
			storytempl.RadioInlineArgType("inlineRadioValue",
				[]string{"flim", "flam", "floop"}),
		).
		AddStory("Default")
}
