package components

import "gitlab.com/joeslazaro/storytempl"

func MultiStoryDemoConf() *storytempl.Conf {
	return storytempl.NewConf("Complex/Multi-story Demo").
		AddArgTypes(
			storytempl.TextArgType("label"),
			storytempl.SelectArgType("className",
				[]string{"primary", "secondary", "danger"}),
		).
		AddDefaultArgs(
			storytempl.TextArg("label", "Click Me"),
			storytempl.SelectArg("className", "primary"),
		).
		AddStory("Default").
		AddStory("Secondary", storytempl.SelectArg("className", "secondary")).
		AddStory("Danger", storytempl.SelectArg("className", "danger"))
}
