package components

import "gitlab.com/joeslazaro/storytempl"

func ColorArgDemoStoryConf() *storytempl.Conf {
	return storytempl.NewConf("Simple/ColorArg").
		AddArgTypes(
			storytempl.ColorArgType("textColor", &[]string{"black", "white"}),
		).
		AddArgTypes(storytempl.ColorArgType("backgroundColor", nil)).
		AddStory("Default",
			storytempl.ColorArg("textColor", "black"),
			storytempl.ColorArg("backgroundColor", "lightgrey"),
		)
}
