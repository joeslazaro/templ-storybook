package components

import "gitlab.com/joeslazaro/storytempl"

func FileArgDemoStoryConf() *storytempl.Conf {
	return storytempl.NewConf("Simple/FileArg").
		AddArgTypes(
			storytempl.FileArgType("urls", ".png"),
		).
		AddStory("Default")
}
