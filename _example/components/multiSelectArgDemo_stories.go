package components

import "gitlab.com/joeslazaro/storytempl"

func MultiSelectArgDemoStoryConf() *storytempl.Conf {
	return storytempl.NewConf("Simple/MultiSelect").
		AddArgTypes(
			storytempl.SelectMultiArgType("selected",
				[]string{"red", "green", "blue"}),
		).
		AddStory("Default")
}
