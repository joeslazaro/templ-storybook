package main

// SPDX-License-Identifier: MIT

import (
	"path/filepath"
	"runtime"

	"gitlab.com/joeslazaro/storytempl"
	"gitlab.com/joeslazaro/storytempl/_example/components"
)

func main() {
	sbook := storytempl.New()
	sbook.UserStaticFilesDir = filepath.Join(currDir(), "static")
	sbook.Header = `<link rel="stylesheet" href="/static/css/main.css">`
	sbook.AddComponent(components.TextArgDemo, components.TextArgDemoStoryConf()).
		AddComponent(components.BooleanArgDemo,
			components.BooleanArgDemoStoryConf()).
		AddComponent(components.ColorArgDemo, components.ColorArgDemoStoryConf()).
		AddComponent(components.MultiStoryDemo, components.MultiStoryDemoConf()).
		AddComponent(components.NumberArgDemo, components.NumberArgDemoStoryConf()).
		AddComponent(components.DateArgDemo, components.DateArgDemoStoryConf()).
		AddComponent(components.CheckArgDemo, components.CheckArgDemoStoryConf()).
		AddComponent(components.RadioArgDemo, components.RadioArgDemoStoryConf()).
		AddComponent(components.MultiSelectArgDemo,
			components.MultiSelectArgDemoStoryConf()).
		AddComponent(components.ObjectArgDemo, components.ObjectArgDemoStoryConf()).
		AddComponent(components.FileArgDemo, components.FileArgDemoStoryConf())

	if err := sbook.ListenAndServe(); err != nil {
		panic(err)
	}
}

func currDir() string {
	var b string
	_, b, _, _ = runtime.Caller(0)
	return filepath.Dir(b)
}
