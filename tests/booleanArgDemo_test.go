package components_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/PuerkitoBio/goquery"
	"github.com/stretchr/testify/require"
	"gitlab.com/joeslazaro/storytempl"
	"gitlab.com/joeslazaro/storytempl/_example/components"
)

func TestBooleanArgDemo(t *testing.T) {
	// component-specific setup
	compo := components.BooleanArgDemo
	conf := components.BooleanArgDemoStoryConf()
	params := url.Values{}
	params.Add("checked", "true")

	sbook := storytempl.New()
	sbook.AddComponent(compo, conf)
	handler := sbook.GetPreviewHandler()

	// prepare the request
	urlPath := fmt.Sprintf("%s?%s", sbook.GetComponentPath(conf), params.Encode())
	req, err := http.NewRequest("GET", urlPath, nil)
	require.NoError(t, err)

	// make the request to the handler
	res := httptest.NewRecorder()
	handler.ServeHTTP(res, req)
	if res.Code != http.StatusOK {
		t.Errorf("bad response code: %v", res.Code)
	}

	// component-specific verification
	doc, err := goquery.NewDocumentFromReader(res.Body)
	require.NoError(t, err)
	element := doc.Find("#example")
	_, isChecked := element.Attr("checked")
	require.True(t, isChecked)
}
