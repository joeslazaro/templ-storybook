package components_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/PuerkitoBio/goquery"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/joeslazaro/storytempl"
	"gitlab.com/joeslazaro/storytempl/_example/components"
)

func TestCheckArgDemo(t *testing.T) {
	// component-specific setup
	compo := components.CheckArgDemo
	conf := components.CheckArgDemoStoryConf()
	var checkValue1, checkValue2 string = "foo", "flim,flam"
	params := url.Values{}
	params.Add("checkValue", checkValue1)
	params.Add("inlineCheckValue", checkValue2)

	sbook := storytempl.New()
	sbook.AddComponent(compo, conf)
	handler := sbook.GetPreviewHandler()

	// prepare the request
	urlPath := fmt.Sprintf("%s?%s", sbook.GetComponentPath(conf), params.Encode())
	req, err := http.NewRequest("GET", urlPath, nil)
	require.NoError(t, err)

	// make the request to the handler
	res := httptest.NewRecorder()
	handler.ServeHTTP(res, req)
	if res.Code != http.StatusOK {
		t.Errorf("bad response code: %v", res.Code)
	}

	// component-specific verification
	doc, err := goquery.NewDocumentFromReader(res.Body)
	require.NoError(t, err)
	element := doc.Find("#check1")
	text := element.Text()
	assert.Equal(t, "check value: foo", text)

	element = doc.Find("#check2")
	text = element.Text()
	assert.Equal(t, "inline check value: flim,flam", text)
}
