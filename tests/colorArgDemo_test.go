package components_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/PuerkitoBio/goquery"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/joeslazaro/storytempl"
	"gitlab.com/joeslazaro/storytempl/_example/components"
)

func TestColorArgDemo(t *testing.T) {
	// component-specific setup
	compo := components.ColorArgDemo
	conf := components.ColorArgDemoStoryConf()
	var testColor, testBgColor string = "black", "lightgrey"
	params := url.Values{}
	params.Add("textColor", testColor)
	params.Add("backgroundColor", testBgColor)

	sbook := storytempl.New()
	sbook.AddComponent(compo, conf)
	handler := sbook.GetPreviewHandler()

	// prepare the request
	urlPath := fmt.Sprintf("%s?%s", sbook.GetComponentPath(conf), params.Encode())
	req, err := http.NewRequest("GET", urlPath, nil)
	require.NoError(t, err)

	// make the request to the handler
	res := httptest.NewRecorder()
	handler.ServeHTTP(res, req)
	if res.Code != http.StatusOK {
		t.Errorf("bad response code: %v", res.Code)
	}

	// component-specific verification
	doc, err := goquery.NewDocumentFromReader(res.Body)
	require.NoError(t, err)
	element := doc.Find("style")
	text := element.Text()
	assert.Contains(t, text, "color:"+testColor)
	assert.Contains(t, text, "background-color:"+testBgColor)
}
