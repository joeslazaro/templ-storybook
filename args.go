package storytempl

// SPDX-License-Identifier: MIT

import (
	"encoding/json"
	"fmt"
	"log/slog"
	"net/url"
	"reflect"
	"strconv"
	"time"
)

type numberControl struct {
	ControlType string `json:"type"`
	Min         *int   `json:"min,omitempty"`
	Max         *int   `json:"max,omitempty"`
	Step        *int   `json:"step,omitempty"`
}

type fileControl struct {
	ControlType string `json:"type"`
	Accept      string `json:"accept"`
}

type colorControl struct {
	ControlType  string    `json:"type"`
	PresetColors *[]string `json:"presetColors,omitempty"`
}

type textControl struct {
	ControlType string `json:"type"`
	MaxLength   int    `json:"maxLength"`
}

type queryParamGetter func(q url.Values) interface{}

type Arg struct {
	Name  string
	Value interface{}
}

type ArgType struct {
	Name    string
	Control interface{}
	Options *[]string
	// Function to retrieve/decode the value from a URL query string
	Get queryParamGetter
	// Optional arg value validator for this arg type
	Validate func(interface{}) error
}

func optionsArgTypeBuilder(controlType string, name string, options []string) ArgType {
	return ArgType{
		Name:    name,
		Control: controlType,
		Options: &options,
		Get: func(q url.Values) interface{} {
			return q.Get(name)
		},
		Validate: func(argValue interface{}) error {
			value, ok := argValue.(string)
			if !ok {
				return fmt.Errorf("For arg '%s', value (%v) is not a string", name, argValue)
			}
			for _, allowedOption := range options {
				if allowedOption == value {
					return nil
				}
			}
			return fmt.Errorf("For arg '%s', option '%s' is not in the list: %v",
				name, value, options)
		},
	}
}

// Provides a select to choose a single value from the options.
func SelectArgType(name string, options []string) ArgType {
	return optionsArgTypeBuilder("select", name, options)
}

// Provides a select to choose any number of the provided options.
func SelectMultiArgType(name string, options []string) ArgType {
	return optionsArgTypeBuilder("multi-select", name, options)
}

// Value for a select/multi-select arg.
func SelectArg(argName string, option string) Arg {
	return Arg{
		Name:  argName,
		Value: option,
	}
}

func ObjectArgType(name string, valuePtr interface{}) ArgType {
	return ArgType{
		Name:    name,
		Control: "object",
		Options: nil,
		Get: func(q url.Values) interface{} {
			err := json.Unmarshal([]byte(q.Get(name)), valuePtr)
			if err != nil {
				return fmt.Errorf("For arg '%s', failed to unmarshal object: %w",
					name, err)
			}
			return reflect.Indirect(reflect.ValueOf(valuePtr)).Interface()
		},
		Validate: nil,
	}
}

// Value for an object arg.
func ObjectArg(name string, value interface{}) Arg {
	return Arg{
		Name:  name,
		Value: value,
	}
}

// Provides a text input.
func TextArgType(name string) ArgType {
	return ArgType{
		Name:    name,
		Control: "text",
		Options: nil,
		Get: func(q url.Values) interface{} {
			return q.Get(name)
		},
		Validate: func(argValue interface{}) error {
			_, ok := argValue.(string)
			if !ok {
				return fmt.Errorf("For arg '%s', value (%v) is not a string",
					name, argValue)
			}
			return nil
		},
	}
}

// Provides a text input with a maximum length.
func TextArgMaxLenType(name string, maxLen int) ArgType {
	control := textControl{
		ControlType: "text",
		MaxLength:   maxLen,
	}
	return ArgType{
		Name:    name,
		Control: control,
		Options: nil,
		Get: func(q url.Values) interface{} {
			return q.Get(name)
		},
		Validate: func(argValue interface{}) error {
			value, ok := argValue.(string)
			if !ok {
				return fmt.Errorf("For arg '%s', value (%v) is not a string",
					name, argValue)
			}
			if len(value) > maxLen {
				return fmt.Errorf("For arg '%s', text exceeds max length %d",
					name, maxLen)
			}
			return nil
		},
	}
}

// Value for a text arg.
func TextArg(name, value string) Arg {
	return Arg{
		Name:  name,
		Value: value,
	}
}

// Provides a toggle for switching between possible states.
func BooleanArgType(name string) ArgType {
	return ArgType{
		Name:    name,
		Control: "boolean",
		Options: nil,
		Get: func(q url.Values) interface{} {
			return q.Get(name) == "true"
		},
		Validate: nil,
	}
}

// Provides a true/false toggle.
func BooleanArg(name string, value bool) Arg {
	return Arg{
		Name:  name,
		Value: value,
	}
}

type NumberArgConf struct{ Min, Max, Step *int }

func NewNumberArgConf() *NumberArgConf {
	minDefault := 0
	stepDefault := 1
	conf := NumberArgConf{
		Min:  &minDefault,
		Max:  nil,
		Step: &stepDefault,
	}
	return &conf
}

func (conf *NumberArgConf) SetMin(min int) *NumberArgConf {
	conf.Min = &min
	return conf
}

func (conf *NumberArgConf) SetMax(max int) *NumberArgConf {
	conf.Max = &max
	return conf
}

func (conf *NumberArgConf) SetStep(step int) *NumberArgConf {
	conf.Step = &step
	return conf
}

// Provides a numeric input with optional min/max/step.
func NumberArgType(name string, conf *NumberArgConf) ArgType {
	if conf == nil {
		conf = NewNumberArgConf()
	}
	control := numberControl{
		ControlType: "number",
		Min:         conf.Min,
		Max:         conf.Max,
		Step:        conf.Step,
	}
	arg := ArgType{
		Name:    name,
		Control: control,
		Options: nil,
		Get: func(q url.Values) interface{} {
			i, _ := strconv.ParseInt(q.Get(name), 10, 64)
			return int(i)
		},
		Validate: func(argValue interface{}) error {
			value, ok := argValue.(int)
			if !ok {
				return fmt.Errorf("For arg '%s', value (%v) is not an int", name, argValue)
			}
			if conf.Min != nil && value < *conf.Min {
				return fmt.Errorf("For arg '%s', value %d is less than min %d", name, value, *conf.Min)
			}
			if conf.Max != nil && value > *conf.Max {
				return fmt.Errorf("For arg '%s', value %d is greater than max %d", name, value, *conf.Max)
			}
			if conf.Step != nil && value%*conf.Step != 0 {
				return fmt.Errorf("For arg '%s', value %d is not a multiple of step %d", name, value, *conf.Step)
			}
			return nil
		},
	}
	return arg
}

// Value for a number arg.
func NumberArg(name string, value int) Arg {
	return Arg{
		Name:  name,
		Value: value,
	}
}

// Provides a numeric range slider with optional min/max/step.
func RangeArgType(name string, conf *NumberArgConf) ArgType {
	arg := NumberArgType(name, conf)
	control := arg.Control.(numberControl)
	control.ControlType = "range"
	arg.Control = control
	return arg
}

// Value for a number arg.
func RangeArg(name string, value int) Arg {
	return Arg{
		Name:  name,
		Value: value,
	}
}

// Provides a float64 numeric input with optional min/max/step.
func FloatArgType(name string, min, max, step float64) ArgType {
	return ArgType{
		Name: name,
		Control: map[string]interface{}{
			"type": "number",
			"min":  min,
			"max":  max,
			"step": step,
		},
		Options: nil,
		Get: func(q url.Values) interface{} {
			i, _ := strconv.ParseFloat(q.Get(name), 64)
			return i
		},
		Validate: func(argValue interface{}) error {
			value, ok := argValue.(float64)
			if !ok {
				return fmt.Errorf("For arg '%s', float value (%v) is not a float", name, argValue)
			}
			if value < min {
				return fmt.Errorf("For arg '%s', float value %f is less than min %f", name, value, min)
			}
			if value > max {
				return fmt.Errorf("For arg '%s', float value %f is greater than max %f", name, value, max)
			}
			if step != 0 && float64(int(value/step)) != value/step {
				return fmt.Errorf("For arg '%s', float value %f is not a multiple of step %f", name, value, step)
			}
			return nil
		},
	}
}

// Value for a float arg.
func FloatArg(name string, value float64) Arg {
	return Arg{
		Name:  name,
		Value: value,
	}
}

// Provides a color picker to choose color values. Can be optionally configured
// to include a set of color presets that show in the color picker.
func ColorArgType(name string, presetColors *[]string) ArgType {
	return ArgType{
		Name: name,
		Control: colorControl{
			ControlType:  "color",
			PresetColors: presetColors,
		},
		Options: nil,
		Get: func(q url.Values) interface{} {
			return q.Get(name)
		},
		Validate: func(argValue interface{}) error {
			value, ok := argValue.(string)
			if !ok {
				return fmt.Errorf("For arg '%s', color value (%v) is not a string", name, argValue)
			}
			if presetColors == nil {
				return nil
			}
			for _, allowedColor := range *presetColors {
				if allowedColor == value {
					return nil
				}
			}
			return fmt.Errorf("For arg '%s', invalid color '%s'", name, value)
		},
	}
}

// Value for a color arg.
func ColorArg(name, value string) Arg {
	return Arg{
		Name:  name,
		Value: value,
	}
}

// Provides a datepicker to choose a date which is returned as a time.Time type.
func DateArgType(name string) ArgType {
	return ArgType{
		Name:    name,
		Control: "date",
		Options: nil,
		Get: func(q url.Values) interface{} {
			timeVal, err := time.Parse(time.RFC3339Nano, q.Get(name))
			if err != nil {
				// If the SB time format ever changes this will need to be updated.
				slog.Warn("Failed to parse date arg '%s' value '%s': %v",
					name, q.Get(name), err)
				return time.Time{}
			}
			return timeVal
		},
		Validate: func(argValue interface{}) error {
			_, ok := argValue.(time.Time)
			if !ok {
				return fmt.Errorf("For arg '%s', value (%v) is not a time.Time",
					name, argValue)
			}
			return nil
		},
	}
}

// Value for a date arg.
func DateArg(name string, value time.Time) Arg {
	return Arg{
		Name:  name,
		Value: value,
	}
}

// Provides a set of stacked checkboxes for selecting multiple options.
func CheckArgType(name string, options []string) ArgType {
	return optionsArgTypeBuilder("check", name, options)
}

// Provides a set of inlined checkboxes for selecting multiple options.
func CheckInlineArgType(name string, options []string) ArgType {
	return optionsArgTypeBuilder("inline-check", name, options)
}

// Value for a check/inline-check arg.
func CheckArg(name string, value string) Arg {
	return Arg{
		Name:  name,
		Value: value,
	}
}

// Provides a set of stacked radio buttons based on the available options.
func RadioArgType(name string, options []string) ArgType {
	return optionsArgTypeBuilder("radio", name, options)
}

// Provides a set of inlined radio buttons based on the available options.
func RadioInlineArgType(name string, options []string) ArgType {
	return optionsArgTypeBuilder("inline-radio", name, options)
}

// Value for a radio/inline-radio arg.
func RadioArg(name string, value string) Arg {
	return Arg{
		Name:  name,
		Value: value,
	}
}

// Provides a file input that returns an array of URLs formatted as a string.
// Can be further customized to accept specific file types.
func FileArgType(name, accept string) ArgType {
	return ArgType{
		Name: name,
		Control: fileControl{
			ControlType: "file",
			Accept:      accept,
		},
		Options: nil,
		Get: func(q url.Values) interface{} {
			return q.Get(name)
		},
		Validate: func(argValue interface{}) error {
			_, ok := argValue.(string)
			if !ok {
				return fmt.Errorf("For arg '%s', value (%v) is not a string",
					name, argValue)
			}
			return nil
		},
	}
}

// Value for a file arg.
func FileArg(name, value string) Arg {
	return Arg{
		Name:  name,
		Value: value,
	}
}
