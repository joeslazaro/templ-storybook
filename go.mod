module gitlab.com/joeslazaro/storytempl

go 1.22.0

require (
	github.com/PuerkitoBio/goquery v1.8.1
	github.com/a-h/pathvars v0.0.14
	github.com/a-h/templ v0.2.648
	github.com/gosimple/slug v1.14.0
	github.com/stretchr/testify v1.9.0
	golang.org/x/mod v0.16.0
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gosimple/unidecode v1.0.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/net v0.19.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
