package util

// SPDX-License-Identifier: MIT

import (
	"bytes"
	"encoding/json"
	"fmt"
	"sync"
)

type OrderedMap[K comparable, V any] struct {
	m        *sync.Mutex
	internal map[K]V
	keys     []K
}

func NewOrderedMap[K comparable, V any]() *OrderedMap[K, V] {
	return &OrderedMap[K, V]{
		m:        new(sync.Mutex),
		internal: map[K]V{},
		keys:     []K{},
	}
}

func (sm *OrderedMap[K, V]) Add(key K, value V) {
	sm.m.Lock()
	defer sm.m.Unlock()
	sm.keys = append(sm.keys, key)
	sm.internal[key] = value
}

func (sm *OrderedMap[K, V]) Get(key K) (V, bool) {
	value, exists := sm.internal[key]
	return value, exists
}

func (sm *OrderedMap[K, V]) Keys() []K {
	return sm.keys
}

func (sm *OrderedMap[K, V]) MarshalJSON() ([]byte, error) {
	sm.m.Lock()
	defer sm.m.Unlock()

	buf := new(bytes.Buffer)
	buf.WriteRune('{')
	enc := json.NewEncoder(buf)

	errTemplate := "error in MarshalJSON(): %w"
	for index, key := range sm.keys {
		err := enc.Encode(key)
		if err != nil {
			return nil, fmt.Errorf(errTemplate, err)
		}

		_, err = buf.WriteRune(':')
		if err != nil {
			return nil, fmt.Errorf(errTemplate, err)
		}

		err = enc.Encode(sm.internal[key])
		if err != nil {
			return nil, fmt.Errorf(errTemplate, err)
		}

		if index < len(sm.keys)-1 {
			_, err = buf.WriteRune(',')
			if err != nil {
				return nil, fmt.Errorf(errTemplate, err)
			}
		}
	}
	buf.WriteRune('}')
	return buf.Bytes(), nil
}
